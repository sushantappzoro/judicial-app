import React from 'react';
import logo from './logo.svg';
import  {BrowserRouter}  from 'react-router-dom';
import {SuperAdminRoutes} from './routes';
import './App.css';

const App = () => {
  return (
    <>
    <BrowserRouter basename="/superadmin">
        <SuperAdminRoutes/>
    </BrowserRouter>

    <BrowserRouter basename="/admin">

    </BrowserRouter>

    <BrowserRouter basename="/clerk">

    </BrowserRouter>
    <BrowserRouter basename="/judge">

    </BrowserRouter>
    <BrowserRouter basename="/solicitor">

    </BrowserRouter>
    <BrowserRouter basename="/defendents">

    </BrowserRouter>
    </>
  );
}

export default App;
