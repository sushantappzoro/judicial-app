import React from 'react';
import Loadable from 'react-loadable'


const Loading = () =>{      
    return <div>Loading...</div>;
}

const SuperAdminDashboard = Loadable({

    loader: () => import('./dashboard'),
    loading: Loading,
});

const SuerAdminLogin = Loadable({

    loader: () => import('./login'),
    loading: Loading,
});



export {
    SuperAdminDashboard,
    SuerAdminLogin
}
