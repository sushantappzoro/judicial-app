import  React ,{lazy,Suspense} from 'react';
import {Router,Route,Switch,Redirect,useLocation} from 'react-router-dom';

import  {SuerAdminLogin,SuperAdminDashboard} from '../views/superadmin';


const SuperAdminRoutes = () =>{

    return(
    <Switch>
        <Route path="/login" exact component={SuerAdminLogin} />
        <Route path="/dashboard" exact component={SuperAdminDashboard} />
    </Switch>
    );

}

export default SuperAdminRoutes;