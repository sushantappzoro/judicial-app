import SuperAdminRoutes from './superdmin';
import AdminRoutes from './admin';
import DefendentRoutes from './defendent';
import ClerkRoutes from './clerk';
import JudgeRoutes from './judge';
import SolicitorRoutes from './solicitor';


export {
    SuperAdminRoutes,
    AdminRoutes,
    DefendentRoutes,
    ClerkRoutes,
    JudgeRoutes,
    SolicitorRoutes
}
